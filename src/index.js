import React from 'react';
import { render } from 'react-dom';
import './index.scss';

const App = () => (
  <div>
    Project management 
  </div>  
);

render(<App />, document.getElementById('root'));
